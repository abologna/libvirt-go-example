package main

import (
	"fmt"
	"os"

	libvirt "libvirt.org/libvirt-go"
)

func main() {
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	doms, err := conn.ListAllDomains(0)
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}

	if len(doms) > 0 {
		fmt.Printf("domains:\n")
		for _, dom := range doms {
			name, err := dom.GetName()
			if err != nil {
				fmt.Printf("%v\n", err)
				os.Exit(1)
			}
			fmt.Printf("  - %s\n", name)
			dom.Free()
		}
	} else {
		fmt.Printf("domains: []\n")
	}
}
