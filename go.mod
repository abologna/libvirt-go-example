module gitlab.com/abologna/libvirt-go-example

go 1.16

require libvirt.org/libvirt-go v7.3.0+incompatible
